import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RemetentesEditarComponent } from './remetentes-editar.component';

describe('RemetentesEditarComponent', () => {
  let component: RemetentesEditarComponent;
  let fixture: ComponentFixture<RemetentesEditarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RemetentesEditarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemetentesEditarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
