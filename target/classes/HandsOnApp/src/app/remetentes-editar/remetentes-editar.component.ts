import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ApiService} from "../api.service";
import {FormBuilder, FormGroup, NgForm, Validators} from "@angular/forms";

@Component({
  selector: 'app-remetentes-editar',
  templateUrl: './remetentes-editar.component.html',
  styleUrls: ['./remetentes-editar.component.css']
})
export class RemetentesEditarComponent implements OnInit {

  remetenteForm:FormGroup;
  id:number=0;
  nome:string='';
  telefone:string='';
  email:string='';
  cpf:string='';
  endereco:string='';
  isLoadingResults = false;

  constructor(private router: Router, private route: ActivatedRoute, private api: ApiService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.getRemetente(Number(this.route.snapshot.params['id']));
    this.remetenteForm = this.formBuilder.group({
      'nome':[null, Validators.required],
      'telefone':[null, Validators.required],
      'email':[null, Validators.required],
      'cpf':[null, Validators.required],
      'endereco':[null, Validators.required]
    });
  }

  getRemetente(id) {
    this.api.getRemetente(id).subscribe(data => {
      this.id = Number(data.id);
      this.remetenteForm.setValue({
        nome: data.nome,
        telefone: data.telefone,
        email: data.email,
        cpf: data.cpf,
        endereco: data.endereco
      });
    });
  }

  onFormSubmit(form:NgForm) {
    this.isLoadingResults = true;
    this.api.updateRemetente(this.id, form)
      .subscribe(res => {
          let id = res['id'];
          this.isLoadingResults = false;
          this.router.navigate(['/remetentes-detalhes', id]);
        }, (err) => {
          console.log(err);
          this.isLoadingResults = false;
        }
      );
  }

  remetenteDetalhes() {
    this.router.navigate(['/remetentes-detalhes', this.id]);
  }

}
