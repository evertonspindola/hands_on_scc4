import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ApiService} from "../api.service";
import {Destinatario} from "../Destinatario";
import {Remetente} from "../Remetente";
import {Router} from "@angular/router";

export interface Remetente {
  id: number;
  nome: string;
  telefone: string;
  cpf: string;
  endereco: string;
  email:string;
}

export interface Destinatario {
  id: number;
  nome: string;
  telefone: string;
  cpf: string;
  endereco: string;
  email:string;
}

@Component({
  selector: 'app-enviar-mensagem',
  templateUrl: './enviar-mensagem.component.html',
  styleUrls: ['./enviar-mensagem.component.css']
})
export class EnviarMensagemComponent implements OnInit {
  remetenteForm:FormGroup;
  destinatarios: Destinatario[];
  remetentes: Remetente[];
  mensagem='';
  isLoadingResults = false;

  constructor(private router: Router, private api: ApiService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.isLoadingResults = true;
    this.initDestinatarios();
    this.initRemetentes()
    this.remetenteForm = this.formBuilder.group({
      'remetentes':[this.remetentes, Validators.required],
      'destinatarios':[this.destinatarios, Validators.required],
      'mensagem':[this.mensagem, Validators.required],
    });
  }

  initDestinatarios(){
    this.api.getDestinatarios()
      .subscribe(res => {
        this.destinatarios = res;
        console.log(this.destinatarios);
        this.isLoadingResults = false;
      }, err => {
        console.log(err);
        this.isLoadingResults = false;
      });
  }

  initRemetentes(){
    this.api.getRemetentes()
      .subscribe(res => {
        this.remetentes = res;
        console.log(this.remetentes);
        this.isLoadingResults = false;
      }, err => {
        console.log(err);
        this.isLoadingResults = false;
      });
  }

  onFormSubmit(value: any) {
    console.log(value);
  }
}
