import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { Remetente } from './Remetente';
import {Destinatario} from './Destinatario'
import { Injectable } from '@angular/core';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

const apiUrlRemetentes = "http://localhost:8080/remetentes";
const apiUrlDestinatarios = "http://localhost:8080/destinatarios";

@Injectable({
  providedIn: 'root',
})
export class ApiService {

  constructor(private http: HttpClient) {
    console.log(http);
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  getRemetentes (): Observable<Remetente[]> {
    return this.http.get<Remetente[]>(apiUrlRemetentes)
      .pipe(
        tap(heroes => console.log('fetched remetentes')),
        catchError(this.handleError('getremetentes', []))
      );
  }

  getDestinatarios (): Observable<Destinatario[]> {
    return this.http.get<Destinatario[]>(apiUrlDestinatarios)
      .pipe(
        tap(heroes => console.log('fetched destinatarios')),
        catchError(this.handleError('getdestinatarios', []))
      );
  }

  getRemetente(id: number): Observable<Remetente> {
    const url = `${apiUrlRemetentes}/${id}`;
    return this.http.get<Remetente>(url).pipe(
      tap(_ => console.log(`fetched remetente id=${id}`)),
      catchError(this.handleError<Remetente>(`getremetente id=${id}`))
    );
  }

  addRemetente (remetente): Observable<Remetente> {
    return this.http.post<Remetente>(apiUrlRemetentes, remetente, httpOptions).pipe(
      tap((remetente: Remetente) => console.log(`added remetente w/ id=${remetente.id}`)),
      catchError(this.handleError<Remetente>('addremetente'))
    );
  }

  updateRemetente (id, remetente): Observable<any> {
    const url = `${apiUrlRemetentes}/${id}`;
    return this.http.put(url, remetente, httpOptions).pipe(
      tap(_ => console.log(`updated remetente id=${id}`)),
      catchError(this.handleError<any>('updateremetente'))
    );
  }

  deleteRemetente (id): Observable<Remetente> {
    const url = `${apiUrlRemetentes}/${id}`;
    return this.http.delete<Remetente>(url, httpOptions).pipe(
      tap(_ => console.log(`deleted remetente id=${id}`)),
      catchError(this.handleError<Remetente>('deleteremetente'))
    );
  }

  addDestinatario (destinatario){
    var json = JSON.stringify(destinatario);
    console.log(json);
    this.http.post(apiUrlDestinatarios,json,httpOptions).subscribe(
      data => console.log("success!", data),
      error => console.error("couldn't post because", error)
    );
  }
}
