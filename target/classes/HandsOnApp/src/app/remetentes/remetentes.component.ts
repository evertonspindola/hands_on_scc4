import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import {Remetente} from "../Remetente";

@Component({
  selector: 'app-remetentes',
  templateUrl: './remetentes.component.html',
  styleUrls: ['./remetentes.component.css']
})
export class RemetentesComponent implements OnInit {
  displayedColumns: string[] = ['remetente_nome', 'remetente_telefone','remetente_email'];
  data: Remetente[] = [];
  isLoadingResults = true;
  constructor(private api: ApiService) { }

  ngOnInit() {
    this.api.getRemetentes()
      .subscribe(res => {
        this.data = res;
        console.log(this.data);
        this.isLoadingResults = false;
      }, err => {
        console.log(err);
        this.isLoadingResults = false;
      });
  }
}
