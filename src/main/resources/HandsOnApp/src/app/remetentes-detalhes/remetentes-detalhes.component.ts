import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ApiService} from "../api.service";
import {Remetente} from "../Remetente";

@Component({
  selector: 'app-remetentes-detalhes',
  templateUrl: './remetentes-detalhes.component.html',
  styleUrls: ['./remetentes-detalhes.component.css']
})
export class RemetentesDetalhesComponent implements OnInit {

  remetente: Remetente = {id: 0,nome:'',telefone:'', cpf:'',email:'',endereco:'' };
  isLoadingResults = true;

  constructor(private route: ActivatedRoute, private api: ApiService, private router: Router) { }

  ngOnInit() {
    this.getDetalhesRemetente(this.route.snapshot.params['id']);
  }

  getDetalhesRemetente(id) {
    this.api.getRemetente(id)
      .subscribe(data => {
        this.remetente = data;
        console.log(this.remetente);
        this.isLoadingResults = false;
      });
  }

  deleteRemetente(id) {
    this.isLoadingResults = true;
    this.api.deleteRemetente(id)
      .subscribe(res => {
          this.isLoadingResults = false;
          this.router.navigate(['/remetentes']);
        }, (err) => {
          console.log(err);
          this.isLoadingResults = false;
        }
      );
  }
}
