import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RemetentesDetalhesComponent } from './remetentes-detalhes.component';

describe('RemetentesDetalhesComponent', () => {
  let component: RemetentesDetalhesComponent;
  let fixture: ComponentFixture<RemetentesDetalhesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RemetentesDetalhesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemetentesDetalhesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
