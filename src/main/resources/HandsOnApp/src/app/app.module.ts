import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { RemetentesComponent } from './remetentes/remetentes.component';
import { RemetentesDetalhesComponent } from './remetentes-detalhes/remetentes-detalhes.component';
import { RemetentesAdicionarComponent } from './remetentes-adicionar/remetentes-adicionar.component';
import { RemetentesEditarComponent } from './remetentes-editar/remetentes-editar.component';
import { HttpClientModule } from '@angular/common/http';
import {AppRoutingModule} from "./app-routing.module";
import {
  MatOptionModule,
  MatSelectModule,
  MatInputModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatSortModule,
  MatTableModule,
  MatIconModule,
  MatButtonModule,
  MatCardModule,
  MatFormFieldModule } from "@angular/material";
import {BrowserModule} from "@angular/platform-browser";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {IConfig, NgxMaskModule} from 'ngx-mask';
import { DestinatariosImportarComponent } from './destinatarios-importar/destinatarios-importar.component';
import { EnviarMensagemComponent } from './enviar-mensagem/enviar-mensagem.component';

@NgModule({
  declarations: [
    AppComponent,
    RemetentesComponent,
    RemetentesDetalhesComponent,
    RemetentesAdicionarComponent,
    RemetentesEditarComponent,
    DestinatariosImportarComponent,
    EnviarMensagemComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatOptionModule,
    MatSelectModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    NgxMaskModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent],
})

export class AppModule {

}
