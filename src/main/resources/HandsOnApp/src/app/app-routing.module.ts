import { NgModule } from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {RemetentesAdicionarComponent} from './remetentes-adicionar/remetentes-adicionar.component';
import {RemetentesComponent} from './remetentes/remetentes.component';
import {RemetentesDetalhesComponent} from './remetentes-detalhes/remetentes-detalhes.component';
import {RemetentesEditarComponent} from './remetentes-editar/remetentes-editar.component';
import {DestinatariosImportarComponent} from "./destinatarios-importar/destinatarios-importar.component";
import {EnviarMensagemComponent} from "./enviar-mensagem/enviar-mensagem.component";

const routes: Routes = [
  {
    path:"enviar-mensagem",
    component:EnviarMensagemComponent,
    data:{title: "Enviar Mensagem"}
  },
  {
    path:"destinatarios-importar",
    component: DestinatariosImportarComponent,
    data:{title: "Importar destinatarios"}
  },
  {
    path:"destinatarios",
    component: DestinatariosImportarComponent,
    data:{title: "Importar destinatarios"}
  },
  {
    path: 'remetentes',
    component: RemetentesComponent,
    data: { title: 'Lista de Remetentes' }
  },
  {
    path: 'remetentes-detalhes/:id',
    component: RemetentesDetalhesComponent,
    data: { title: 'Detalhes de Remetentes' }
  },
  {
    path: 'remetentes-adicionar',
    component: RemetentesAdicionarComponent,
    data: { title: 'Adicionar Remetentes' }
  },
  {
    path: 'remetentes-editar/:id',
    component: RemetentesEditarComponent,
    data: { title: 'Editar Remetentes' }
  },
  { path: '',
    redirectTo: '/remetentes',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
