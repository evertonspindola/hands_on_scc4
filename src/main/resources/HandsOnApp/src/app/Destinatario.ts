export class Destinatario {
  id: number;
  nome: string;
  telefone: string;
  cpf: string;
  endereco: string;
  email:string;
}
