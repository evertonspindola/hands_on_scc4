import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DestinatariosImportarComponent } from './destinatarios-importar.component';

describe('DestinatariosImportarComponent', () => {
  let component: DestinatariosImportarComponent;
  let fixture: ComponentFixture<DestinatariosImportarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DestinatariosImportarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DestinatariosImportarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
