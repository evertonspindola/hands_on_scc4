import {Component, OnInit, ViewChild} from '@angular/core';
import {ApiService} from "../api.service";
import {Destinatario} from "../Destinatario";

export interface CSVDestinatario{
  id: number;
  nome: string;
  telefone: string;
  cpf: string;
  endereco: string;
  email:string;
}

@Component({
  selector: 'app-destinatarios-importar',
  templateUrl: './destinatarios-importar.component.html',
  styleUrls: ['./destinatarios-importar.component.css']
})
export class DestinatariosImportarComponent implements OnInit {

  displayedColumns: string[] = ['nome', 'telefone', 'email', 'cpf','endereco'];
  dataSource: CSVDestinatario[];
  data: Destinatario[];
  isLoadingResults = false;

  constructor(private api: ApiService){}

  ngOnInit(): void {
    this.initDestinatarios();
  }

  initDestinatarios(){
    this.api.getDestinatarios()
      .subscribe(res => {
        this.data = res;
        console.log(this.data);
        this.isLoadingResults = false;
      }, err => {
        console.log(err);
        this.isLoadingResults = false;
      });
  }

  @ViewChild('csvReader',{static:false}) csvReader: any;

  uploadListener($event: any): void {
    let files = $event.srcElement.files;

    if (this.isValidCSVFile(files[0])) {
      this.isLoadingResults = true;
      let input = $event.target;
      let reader = new FileReader();
      reader.readAsText(input.files[0]);

      reader.onload = () => {
        let csvData = reader.result;
        let csvDestinatariosArray = (<string>csvData).split(/\r\n|\n/);

        this.dataSource = this.getDataRecordsArrayFromCSVFile(csvDestinatariosArray);
        this.isLoadingResults = false;
      };

      reader.onerror = function () {
        console.log('error is occured while reading file!');
      };

    } else {
      alert("Please import valid .csv file.");
      this.fileReset();
    }
  }

  getDataRecordsArrayFromCSVFile(csvDestinatariosArray: any) {
    let csvArr = [];
    for (let i = 1; i < csvDestinatariosArray.length; i++) {
      let currentRecord = (<string>csvDestinatariosArray[i]).split(',');
      if (currentRecord.length == 5) {
        let destinatario = {nome:currentRecord[0].trim(),'telefone':currentRecord[1].trim(),'email':currentRecord[2].trim(),'cpf':currentRecord[3].trim(),'endereco':currentRecord[4].trim()}
        csvArr.push(destinatario);
      }
    }
    return csvArr;
  }

  isValidCSVFile(file: any) {
    return file.name.endsWith(".csv");
  }

  fileReset() {
    this.csvReader.nativeElement.value = "";
    this.dataSource = [];
  }

  Importar() {
    console.log("Rows: "+this.dataSource.length);
    this.dataSource.forEach((destinatario)=>{
      console.log("destinatario: "+destinatario.nome);
      this.api.addDestinatario(destinatario);
    })
    alert("Destinatários importados com sucesso.");
    this.initDestinatarios();
  }

  enableButton() {
    if(this.dataSource != null){
      console.log(this.dataSource.length > 0);
      return this.dataSource.length > 0;
    }
    return false;
  }
}
