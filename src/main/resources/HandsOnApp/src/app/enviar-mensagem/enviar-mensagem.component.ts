import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, NgForm, Validators} from '@angular/forms';
import {ApiService} from "../api.service";
import {Router} from "@angular/router";

export interface Remetente {
  id: number;
  nome: string;
  telefone: string;
  cpf: string;
  endereco: string;
  email:string;
}

export interface Destinatario {
  id: number;
  nome: string;
  telefone: string;
  cpf: string;
  endereco: string;
  email:string;
}

@Component({
  selector: 'app-enviar-mensagem',
  templateUrl: './enviar-mensagem.component.html',
  styleUrls: ['./enviar-mensagem.component.css']
})
export class EnviarMensagemComponent implements OnInit {
  remetenteForm:FormGroup;
  destinatarios: Destinatario[];
  remetentes: Remetente[];
  mensagem='';
  isLoadingResults = false;
  log = '';

  constructor(private router: Router, private api: ApiService, private formBuilder: FormBuilder) {
    this.remetenteForm = this.formBuilder.group({
      'remetente':[null, Validators.required],
      'destinatarios':[null, Validators.required],
      'mensagem':[null, Validators.required],
    });
  }

  ngOnInit(): void {
    this.initRemetentes();
    this.initDestinatarios();
  }

  initDestinatarios(){
    this.api.getDestinatarios()
      .subscribe(res => {
        this.destinatarios = res;
        console.log(this.destinatarios);
        this.isLoadingResults = false;
      }, err => {
        console.log(err);
        this.isLoadingResults = false;
      });
  }

  initRemetentes(){
    this.api.getRemetentes()
      .subscribe(res => {
        this.remetentes = res;
        console.log(this.remetentes);
        this.isLoadingResults = false;
      }, err => {
        console.log(err);
        this.isLoadingResults = false;
      });
  }

  onFormSubmit(form:NgForm) {
    console.log(form);
    let remetente = form['remetente'];
    let destinatarios = form['destinatarios'];
    let mensagem = form['mensagem'];
    if (destinatarios != null) {
      destinatarios.forEach((destinatario)=>{
        this.log += remetente+" enviou mensagem para "+destinatario.nome + " - Mensagem: "+mensagem+'\n';
      })
    }
  }
}
