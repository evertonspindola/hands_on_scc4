import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RemetentesAdicionarComponent } from './remetentes-adicionar.component';

describe('RemetentesAdicionarComponent', () => {
  let component: RemetentesAdicionarComponent;
  let fixture: ComponentFixture<RemetentesAdicionarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RemetentesAdicionarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemetentesAdicionarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
