import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {ApiService} from "../api.service";
import {FormBuilder, FormGroup, NgForm, Validators} from "@angular/forms";

@Component({
  selector: 'app-remetentes-adicionar',
  templateUrl: './remetentes-adicionar.component.html',
  styleUrls: ['./remetentes-adicionar.component.css']
})
export class RemetentesAdicionarComponent implements OnInit {

  remetenteForm:FormGroup;
  isLoadingResults = false;
  nome:string='';
  telefone:string='';
  email:string='';
  cpf:string='';
  endereco:string='';


  constructor(private router: Router, private api: ApiService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.remetenteForm = this.formBuilder.group({
      'nome':[null, Validators.required],
      'telefone':[null, Validators.required],
      'email':[null, Validators.required],
      'cpf':[null, Validators.required],
      'endereco':[null, Validators.required]
    });
  }

  onFormSubmit(form:NgForm) {
    this.isLoadingResults = true;
    this.api.addRemetente(form)
      .subscribe(res => {
        let id = res['id'];
        this.isLoadingResults = false;
        this.router.navigate(['/remetentes-detalhes', id]);
      }, (err) => {
        console.log(err);
        this.isLoadingResults = false;
      });
  }
}
