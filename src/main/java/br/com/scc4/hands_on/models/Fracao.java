package br.com.scc4.hands_on.models;

import br.com.scc4.hands_on.models.DTO.FracaoDTO;

public class Fracao {
    private FracaoDTO f1;
    private FracaoDTO f2;

    public FracaoDTO getF1() {
        return f1;
    }

    public void setF1(FracaoDTO f1) {
        this.f1 = f1;
    }

    public FracaoDTO getF2() {
        return f2;
    }

    public void setF2(FracaoDTO f2) {
        this.f2 = f2;
    }
}
