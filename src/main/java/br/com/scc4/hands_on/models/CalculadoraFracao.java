package br.com.scc4.hands_on.models;

import br.com.scc4.hands_on.models.DTO.FracaoDTO;

public class CalculadoraFracao {

    public static FracaoDTO soma(FracaoDTO f1, FracaoDTO f2) {
        validacaoDivisaoPorZero(f1, f2);
        int mdc = menorDenominadorComum(f1.getDenominador(), f2.getDenominador());
        FracaoDTO mdcA = converter(f1, mdc);
        FracaoDTO mdcB = converter(f2, mdc);
        FracaoDTO sum = new FracaoDTO();
        sum.setNumerador(mdcA.getNumerador() + mdcB.getNumerador());
        sum.setDenominador(mdc);
        return reducao(sum);
    }

    public static FracaoDTO subtracao(FracaoDTO f1, FracaoDTO f2) {
        validacaoDivisaoPorZero(f1, f2);
        int mdc = menorDenominadorComum(f1.getDenominador(), f2.getDenominador());
        FracaoDTO mdcA = converter(f1,mdc);
        FracaoDTO mdcB = converter(f2,mdc);
        FracaoDTO diff = new FracaoDTO();
        diff.setNumerador(mdcA.getNumerador() - mdcB.getNumerador());
        diff.setDenominador(mdc);
        return reducao(diff);
    }

    public static FracaoDTO multiplicacao(FracaoDTO f1, FracaoDTO f2) {
        validacaoDivisaoPorZero(f1, f2);
        FracaoDTO produto = new FracaoDTO();
        produto.setNumerador(f1.getNumerador() * f2.getNumerador());
        produto.setDenominador(f1.getDenominador() * f2.getDenominador());
        return reducao(produto);
    }

    public static FracaoDTO divisao(FracaoDTO f1, FracaoDTO f2) {
        validacaoDivisaoPorZero(f1, f2);
        FracaoDTO resultado = new FracaoDTO();
        resultado.setNumerador(f1.getNumerador() * f2.getDenominador());
        resultado.setDenominador(f1.getDenominador() * f2.getNumerador());
        return reducao(resultado);
    }

    public static void validacaoDivisaoPorZero(FracaoDTO f1, FracaoDTO f2) {
        if ((f1.getDenominador() == 0) || (f2.getDenominador() == 0))
            throw new IllegalArgumentException("denominador não pode ser zero(0)");
    }

    private static int menorDenominadorComum(int denom1, int denom2) {
        int factor = denom1;
        while ((denom1 % denom2) != 0)
            denom1 += factor;
        return denom1;
    }

    private static int maiorDenominadorComum(int denom1, int denom2) {
        int factor = denom2;
        while (denom2 != 0) {
            factor = denom2;
            denom2 = denom1 % denom2;
            denom1 = factor;
        }
        return denom1;
    }

    private static FracaoDTO converter(FracaoDTO fracaoDTO,int mdc) {
        FracaoDTO resultado = new FracaoDTO();
        int fator = mdc / fracaoDTO.getDenominador();
        resultado.setNumerador(fracaoDTO.getNumerador() * fator);
        resultado.setDenominador(mdc);
        return resultado;
    }

    private static FracaoDTO reducao(FracaoDTO fracaoDTO) {
        FracaoDTO resultado = new FracaoDTO();
        int mdc = 0;
        int num = Math.abs(fracaoDTO.getNumerador());
        int den = Math.abs(fracaoDTO.getDenominador());
        if (num > den)
            mdc = maiorDenominadorComum(num, den);
        else if (num < den)
            mdc = maiorDenominadorComum(den, num);
        else
            mdc = num;

        resultado.setNumerador(fracaoDTO.getNumerador() / mdc);
        resultado.setDenominador(fracaoDTO.getDenominador() / mdc);
        return resultado;
    }
}
