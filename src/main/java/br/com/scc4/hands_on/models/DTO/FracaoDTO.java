package br.com.scc4.hands_on.models.DTO;

public class FracaoDTO {

    public FracaoDTO() {}

    public FracaoDTO(int numerador, int denominador) {
        this.numerador = numerador;
        this.denominador = denominador;
    }

    private int numerador;

    private int denominador;

    public int getNumerador() {
        return numerador;
    }

    public void setNumerador(int numerador) {
        this.numerador = numerador;
    }

    public int getDenominador() {
        return denominador;
    }

    public void setDenominador(int denominador) {
        this.denominador = denominador;
    }

    public String toString() {
        String buffer = numerador + "/" + denominador;
        return buffer;
    }
}
