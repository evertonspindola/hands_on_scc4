package br.com.scc4.hands_on.controllers;

import br.com.scc4.hands_on.Data.Remetente;
import br.com.scc4.hands_on.Data.IRemetenteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/remetentes")
public class RemetenteController {

    private final IRemetenteRepository remetenteRepository;

    @Autowired
    public RemetenteController(IRemetenteRepository IRemetenteRepository) {
        this.remetenteRepository = IRemetenteRepository;
    }

    @GetMapping
    public List findAll(){
        return remetenteRepository.findAll();
    }

    @GetMapping(path = {"/{id}"})
    public ResponseEntity findById(@PathVariable long id){
        return remetenteRepository.findById(id)
                .map(record -> ResponseEntity.ok().body(record))
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    public Remetente create(@RequestBody Remetente remetente){
        return remetenteRepository.save(remetente);
    }

    @PutMapping(value="/{id}")
    public ResponseEntity update(@PathVariable("id") long id,
                                 @RequestBody Remetente remetente) {
        return remetenteRepository.findById(id)
                .map(record -> {
                    record.setNome(remetente.getNome());
                    record.setEmail(remetente.getEmail());
                    record.setTelefone(remetente.getTelefone());
                    record.setCpf(remetente.getCpf());
                    record.setEndereco(remetente.getEndereco());
                    Remetente updated = remetenteRepository.save(record);
                    return ResponseEntity.ok().body(updated);
                }).orElse(ResponseEntity.notFound().build());
    }

    @DeleteMapping(path ={"/{id}"})
    public ResponseEntity<?> delete(@PathVariable long id) {
        return remetenteRepository.findById(id)
                .map(record -> {
                    remetenteRepository.deleteById(id);
                    return ResponseEntity.ok().build();
                }).orElse(ResponseEntity.notFound().build());
    }

}
