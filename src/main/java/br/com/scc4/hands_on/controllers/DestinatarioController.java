package br.com.scc4.hands_on.controllers;

import br.com.scc4.hands_on.Data.Destinatario;
import br.com.scc4.hands_on.Data.IDestinatarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/destinatarios")
public class DestinatarioController {

    private final IDestinatarioRepository destinatarioRepository;

    @Autowired
    public DestinatarioController(IDestinatarioRepository destinatarioRepository) {
        this.destinatarioRepository = destinatarioRepository;
    }

    @GetMapping
    public List findAll(){
        return destinatarioRepository.findAll();
    }

    @GetMapping(path = {"/{id}"})
    public ResponseEntity findById(@PathVariable long id){
        return destinatarioRepository.findById(id)
                .map(record -> ResponseEntity.ok().body(record))
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    public Destinatario create(@RequestBody Destinatario destinatario){
        return destinatarioRepository.save(destinatario);
    }

    @PutMapping(value="/{id}")
    public ResponseEntity update(@PathVariable("id") long id,
                                 @RequestBody Destinatario destinatario) {
        return destinatarioRepository.findById(id)
                .map(record -> {
                    record.setNome(destinatario.getNome());
                    record.setEmail(destinatario.getEmail());
                    record.setTelefone(destinatario.getTelefone());
                    record.setCpf(destinatario.getCpf());
                    record.setEndereco(destinatario.getEndereco());
                    Destinatario updated = destinatarioRepository.save(record);
                    return ResponseEntity.ok().body(updated);
                }).orElse(ResponseEntity.notFound().build());
    }

    @DeleteMapping(path ={"/{id}"})
    public ResponseEntity<?> delete(@PathVariable long id) {
        return destinatarioRepository.findById(id)
                .map(record -> {
                    destinatarioRepository.deleteById(id);
                    return ResponseEntity.ok().build();
                }).orElse(ResponseEntity.notFound().build());
    }

}
