package br.com.scc4.hands_on.controllers;

import br.com.scc4.hands_on.models.CalculadoraFracao;
import br.com.scc4.hands_on.models.DTO.FracaoDTO;
import br.com.scc4.hands_on.models.Fracao;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
public class ParteUmController {

	//Dado uma lista de números, imprimir a lista de trás pra frente.
	//GET http://localhost:8080/listaReversa?lista={25,35,40,21}
	@GetMapping("/listaReversa")
	public String listaReversa(String lista) {
        return converteVetorEmStringNoFormatoEsperado(retornaUmaListaReversa(lista));
	}

	//Dado uma lista de números, imprimir todos os numeros impares
	//GET http://localhost:8080/imprimirImpares?lista={25,35,40,21}
	@GetMapping("/imprimirImpares")
	public String imprimirImpares(String lista){
		return converteVetorEmStringNoFormatoEsperado(obterNumerosImpares(lista));
	}

	@GetMapping("/imprimirPares")
	public String imprimirPares(String lista){
		return converteVetorEmStringNoFormatoEsperado(obterNumerosPares(lista));
	}

	@GetMapping("/tamanho")
	public String tamanho(String palavra){
		return "tamanho="+palavra.length();
	}

	@GetMapping("/maiusculas")
	public String maiusculas(String palavra){
		return palavra.toUpperCase();
	}

	@GetMapping("/vogais")
	public String vogais(String palavra){
		String filter = "[^aeiouAEIOU]";
		return palavra.replaceAll(filter, "");
	}

	@GetMapping("/consoantes")
	public String consoantes(String palavra){
		String filter = "[aeiouAEIOU]";
		return palavra.replaceAll(filter, "");
	}

	@GetMapping(value = "/nomeBibliografico")
	public String nomeBibliografico(String nome){
		String [] nomes = nome.split(" ");
		String concatenacao = nomes[nomes.length-1].toUpperCase()+",";
		for (int i = 0; i < nomes.length-1; i++) {
			concatenacao += " "+StringUtils.capitalize(nomes[i]);
		}
		return concatenacao;
	}

	@GetMapping(value = "/saque")
	public String saque(int valor){
		if(valor < 8)
			return "Saque mínimo R$: 8";
		int contaNota5 = 0;
		int contaNota3 = 0;

		contaNota5 = valor / 5;
		contaNota3 = (valor - (contaNota5 * 5)) / 3;
		return "Saque R$8: "+contaNota3+" nota de R$3 e "+contaNota5+" nota de R$5";
	}

	@PostMapping(path = "/fracoes/soma")
	public FracaoDTO soma(@RequestBody Fracao fracao){
		return CalculadoraFracao.soma(fracao.getF1(), fracao.getF2());
	}

	@PostMapping("/fracoes/subtracao")
	public FracaoDTO subtracao(@RequestBody Fracao fracao){
		return CalculadoraFracao.subtracao(fracao.getF1(), fracao.getF2());
	}

	@PostMapping("/fracoes/multiplicacao")
	public FracaoDTO multiplicacao(@RequestBody Fracao fracao){
		return CalculadoraFracao.multiplicacao(fracao.getF1(), fracao.getF2());
	}

	@PostMapping("/fracoes/divisao")
	public FracaoDTO divisao(@RequestBody Fracao fracao){
		return CalculadoraFracao.divisao(fracao.getF1(), fracao.getF2());
	}

	private Integer[] obterNumerosPares(String lista) {
		List<Integer> pares = new ArrayList<>();
		Integer[] listaReversa = retornaUmaListaReversa(lista);
		for (Integer number: listaReversa) {
			if(number % 2 == 0){
				pares.add(number);
			}
		}
		Integer[] retorno = new Integer[pares.size()];
		return pares.toArray(retorno);
	}

	private Integer[] obterNumerosImpares(String lista){
		List<Integer> impares = new ArrayList<>();
		Integer[] listaReversa = retornaUmaListaReversa(lista);
		for (Integer number: listaReversa) {
			if(number % 2 != 0){
				impares.add(number);
			}
		}
		Integer[] retorno = new Integer[impares.size()];
		return impares.toArray(retorno);
	}

	private Integer[] retornaUmaListaReversa(String lista){
		Integer[] numeros;
		Integer[] listaReversa = new Integer[0];
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			lista = lista.substring(1,lista.length() -1);
			numeros = objectMapper.readValue("["+lista+"]", Integer[].class);
			int position = numeros.length-1;
			listaReversa = new Integer[numeros.length];
			for (Integer number: numeros) {
				listaReversa[position] = number;
				position--;
			}
		}catch (IOException e){
			e.printStackTrace();
		}
		return listaReversa;
	}

	private String converteVetorEmStringNoFormatoEsperado(Integer[] lista){
		String listaReversaTextList =  Arrays.toString(lista);
		return "{"+listaReversaTextList.substring(1,listaReversaTextList.length()-1)+"}";
	}
}
