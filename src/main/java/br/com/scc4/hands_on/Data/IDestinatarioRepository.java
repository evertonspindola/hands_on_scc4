package br.com.scc4.hands_on.Data;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IDestinatarioRepository extends JpaRepository<Destinatario,Long> {}
