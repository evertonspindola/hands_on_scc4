package br.com.scc4.hands_on;

import br.com.scc4.hands_on.Data.Destinatario;
import br.com.scc4.hands_on.Data.IDestinatarioRepository;
import br.com.scc4.hands_on.Data.Remetente;
import br.com.scc4.hands_on.Data.IRemetenteRepository;
import br.com.scc4.hands_on.models.DTO.FracaoDTO;
import com.google.gson.Gson;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.beans.PropertyEditorSupport;
import java.util.stream.Stream;

@SpringBootApplication
public class HandsOnApplication {

    public static void main(String[] args) {
        SpringApplication.run(HandsOnApplication.class, args);
    }

    @Bean
    CommandLineRunner initRemetente(IRemetenteRepository IRemetenteRepository, IDestinatarioRepository IDestinatarioRepository) {
        return args -> {
            Stream.of("João", "Maria", "Fulana", "Cicrano", "Beltrano").forEach(nome -> {
                Remetente remetente = new Remetente(nome,nome.toLowerCase() + "@domain.com","(49)99999-999"+IRemetenteRepository.count());
                IRemetenteRepository.save(remetente);
            });
            IRemetenteRepository.findAll().forEach(System.out::println);
            Stream.of("Kristofer", "Jimmie", "Caroline", "Dan", "Nancy").forEach(nome -> {
                Destinatario remetente = new Destinatario(nome,nome.toLowerCase() + "@domain.com","(49)99999-999"+IDestinatarioRepository.count());
                IDestinatarioRepository.save(remetente);
            });
            IDestinatarioRepository.findAll().forEach(System.out::println);
        };
    }

    @Bean
    public ConfigurableServletWebServerFactory webServerFactory() {
        TomcatServletWebServerFactory factory = new TomcatServletWebServerFactory();
        factory.addConnectorCustomizers(connector -> connector.setProperty("relaxedQueryChars", "{}"));
        return factory;
    }

    @Bean
    WebMvcConfigurer webMvcConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addResourceHandlers(ResourceHandlerRegistry registry) {
                registry.addResourceHandler("/static/**")
                        .addResourceLocations("classpath:/static/");
            }
        };
    }

    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {
        dataBinder.registerCustomEditor(FracaoDTO.class, new PropertyEditorSupport() {
            Object value;
            @Override
            public Object getValue() {
                return value;
            }

            @Override
            public void setAsText(String text) throws IllegalArgumentException {
                value = new Gson().fromJson((String) text, FracaoDTO.class);
            }
        });
    }
}
